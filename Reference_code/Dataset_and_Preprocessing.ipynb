{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Data set and pre-processing\n",
    "\n",
    "The following procedure can be used to reproduce the results that are present in the article \"Nucleotide excision repair is impaired by binding of transcription factors to DNA\"."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following data set are **mandatory** to run the [scripts](Scripts_Execution.ipynb) for mutation and repair rate analysis. And, these files have to be placed inside the ***dataset*** directory\n",
    "\n",
    " * [Somatic mutations](#Somatic mutations)\n",
    " * [Transcription factor binding sites](#TFBS)\n",
    " * [DNase Hypersensitive sites](#DHS)\n",
    " * [Other annotations](#others)\n",
    " * [Excision repair data](#xrseq)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"Somatic mutations\"><h3>1. Somatic mutations</h3></a>\n",
    "\n",
    "The somatic mutations of different cancer types used in this analysis were obtained from [Fredriksson et al., 2014](http://www.ncbi.nlm.nih.gov/pubmed/25383969)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "%%bash \n",
    "\n",
    "# create a directory for dataset\n",
    "mkdir dataset\n",
    "\n",
    "# create a directory called \"mutations\" inside dataset directory and place the mutation file\n",
    "mkdir dataset/mutations\n",
    "\n",
    "# the mutation file \"mutations.tsv.gz\" obtained was already filtered for mutations that overlap with \n",
    "# dbSNP (v138) entries. Further, we seperate single nucleotide substitutions with a minimum variant frequency\n",
    "# of 0.2 (as recommended by the authors of Fredriksson et al.,) for the following cancer types \n",
    "# SKCM, LUAD, LUSC, CRC, BRCA, BLCA and HNSC.\n",
    "\n",
    "for ctype in SKCM LUAD LUSC BRCA BLCA HNSC CRC; do  lctype=`echo $ctype | tr '[:upper:]' '[:lower:]'`; \\\n",
    "    zgrep -w $ctype dataset/mutations/mutations.tsv.gz | awk '$7>0.2' | \\\n",
    "    awk 'BEGIN{OFS=\"\\t\";} {if(length($5)==1 && length($6)==1 && $5!~/-/ && $6!~/-/){print \"chr\"$3,$4,$4,$5,$6,$1}}' | \\\n",
    "    sort -k1,1 -k2,2n | gzip -c >dataset/mutations/${lctype}.txt.gz; done\n",
    "    \n",
    "# compute the number of mutations per sample\n",
    "for i in $(ls dataset/mutations/*.gz);do name=`echo $i | cut -d \"/\" -f 3 | cut -d \".\" -f 1`; \\\n",
    " zcat $i | cut -f 6 | sort | uniq -c | awk -v var=$name 'BEGIN{OFS=\"\\t\";}{print var,$2,$1}' | sort -n -k3,3; \\\n",
    " done >dataset/mutations/perSample_MutationCount.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The above mutation files seperated per cancer type contain 6 columns delimited by tab (similar to BED format):\n",
    "<table align=\"left\">\n",
    "<tr><th>column</th><th>description</th></tr>\n",
    "<tr><td>1</td><td>chromosome name with prefix \"chr\"</td></tr>\n",
    "<tr><td>2</td><td>mutation position (one-based)</td><tr>\n",
    "<tr><td>3</td><td>mutation position (one-based)</td><tr>\n",
    "<tr><td>4</td><td>Reference allele. A single letter: A, C, G or T (upper case)</td><tr>\n",
    "<tr><td>5</td><td>Alternate allele. A single letter: A, C, G or T (upper case)</td><tr>\n",
    "<tr><td>6</td><td>Sample identifier</td><tr> \n",
    "</table>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# example\n",
    "chr1   58553693   58553693   C   T   XXXX "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The somatic mutations from the whole genome of normal human skin sample were obtained from [Martincorena et al., 2015](http://www.ncbi.nlm.nih.gov/pubmed/25999502)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "%%bash\n",
    "\n",
    "# the mutation file \"PD20399be_wg_caveman_annotated_with_dinucs.5cols\" from Marincorena et al., has to be placed\n",
    "# inside dataset/mutations\n",
    "\n",
    "# extract only single nucleotide variants\n",
    "grep -v sampleID dataset/mutations/PD20399be_wg_caveman_annotated_with_dinucs.5cols | \\\n",
    " awk 'BEGIN{OFS=\"\\t\";} {if(length($4)==1 && length($5)==1){print \"chr\"$2,$3,$3,$4,$5;}}' | gzip >dataset/mutations/eyelid.txt.gz\n",
    "\n",
    "# number of snv counts\n",
    "zcat dataset/mutations/eyelid.txt.gz | wc -l | sed \"s/^/skcm\\tnormalSkin\\t/\" >>dataset/mutations/perSample_MutationCount.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"TFBS\"><h3>2. Transcription factor binding sites annotations</h3></a>\n",
    "\n",
    "The genomic coordinates of the transcription factor binding sites used in the analysis can be obtained from http://bg.upf.edu/group/projects/tfbs/."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "%%bash\n",
    "\n",
    "mkdir dataset/TFBS\n",
    "wget -r -nH --cut-dirs=3 --no-parent --reject=\"index.html*\" -e robots=off http://bg.upf.edu/group/projects/tfbs/ \\\n",
    " -P dataset/TFBS"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The files are in BED format (hg19 assembly).\n",
    "<table align=\"left\">\n",
    "<tr><th>column</th><th>description</th></tr>\n",
    "<tr><td>1</td><td>chromosome name with prefix \"chr\"</td></tr>\n",
    "<tr><td>2</td><td>chromosome start position (zero-based)</td></tr>\n",
    "<tr><td>3</td><td>chromosome end position (one-based)</td></tr>\n",
    "<tr><td>4</td><td>TF_motif_name</td></tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h4>Data description</h4>\n",
    "\n",
    "All these transcription factor binding sites (TFBS), i.e., TF motif match under ChIP-seq peak regions, were obtained from ENCODE. These comprised,\n",
    "<ul>\n",
    "<li>binding sites of 109 transcription factors (TF) as used in <a href=\"http://www.ncbi.nlm.nih.gov/pubmed/24092746\">Khurana et al., 2013</a>.</li>\n",
    "<li>ENCODE predicted binding sites of 52 transcription factors which are not supported by ChIP-\n",
    "seq peaks obtained from  <a href=\"http://ftp.ebi.ac.uk/pub/software/ensembl/encode/supplementary/integration_data_jan2011/byFreeze/jan2011/motifs/jan2011/motif_analysis/\">ftp portal</a></li>\n",
    "<li>In addition, we obtained the binding sites of 32 TFs used in <a href=\"http://www.ncbi.nlm.nih.gov/pubmed/25624100\">Reijns et al., 2015</a></li>\n",
    "</ul>\n",
    "\n",
    "The binding sites were classified into active/inactive binding sites based on their overlap with the [DNase Hypersensitive sites](#DHS). Further, we seperate the binding sites, proximal (promoter regions upstream of TSS) and distal (>5kb away from TSS), based on their genomic location.  \n",
    "\n",
    "\n",
    "The different sets of genomic coordinates used for mutation rate analysis:\n",
    "<table align=\"left\">\n",
    "<tr><th>class</th><th>filename</th><th>description</th></tr>\n",
    "<tr><td> active TFBS in proximal regions </td><td> proximalTFBS-DHS_\\*.bed.gz </td><td>TFBS located in the promoter regions and overlap with DHSs</td></tr>\n",
    "<tr><td> inactive TFBS in proximal regions </td><td> proximalTFBS-noDHS_\\*.bed.gz</td><td>TFBS located in the promoter regions but do not overlap with DHSs</td></tr>\n",
    "<tr><td> active TFBS in distal regions </td><td> distalTFBS-DHS_skcm.bed.gz\n",
    " </td><td>TFBS located in the intergenic region with no annotated TSS within 5kb distance on either sides</td></tr>\n",
    "<tr><td> bound/unbound TFBS in proximal regions </td><td> allTFBS_bound\\*.bed.gz, allTFBS_unbound\\*.bed.gz </td><td>TFBS located in the promoter regions and overlap with TF peak (bound) or not (unbound)</td></tr>\n",
    "<tr><td> active TFBS in transcribed regions</td><td> allTFBS_DHS_skcm_templateStrand.bed.gz </td><td>TFBS in template strand of the gene</td></tr>\n",
    "<tr><td> active TFBS in transcribed regions</td><td> allTFBS_DHS_skcm_nontemplateStrand.bed.gz </td><td>TFBS in non-template strand of the gene</td></tr>\n",
    "<tr><td> active TFBS in promoter region</td><td> proximalTFBS-DHS_skcm_quartiles.bed.gz </td><td>TFBS are startified based on ChIP-seq read coverage as provided by Reijns et al., 2015</td></tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"DHS\"><h3>3. DNase Hypersensitive site (DHS) </h3></a>\n",
    "\n",
    "Download the DHSs identified by Hotspot algorith (narrowPeaks in FDR 1%) from Epigenome roadmap portal."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "%%bash\n",
    "\n",
    "mkdir dataset/DHS\n",
    "wget http://egg2.wustl.edu/roadmap/data/byFileType/peaks/consolidated/narrowPeak/{E059,E088,E028,E084}-DNase.hotspot.fdr0.01.peaks.bed.gz \\\n",
    " -P dataset/DHS \n",
    "\n",
    "# matching DHS for each cancer types\n",
    "declare -A arr=([\"skcm\"]=\"E059\" [\"luad\"]=\"E088\" [\"lusc\"]=\"E088\" [\"crc\"]=\"E084\")\n",
    "\n",
    "# save that information in a file\n",
    "echo -e \"#ctype\\troadmapID\\tfile\" >dataset/DHS/DHS_list.txt\n",
    "for i in ${!arr[@]};do echo -e \"$i\\t${arr[$i]}\\t${arr[$i]}-DNase.hotspot.fdr0.01.peaks.bed.gz\";done >>dataset/DHS/DHS_list.txt "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"others\"><h3>4. Other annotations </h3></a>\n",
    "\n",
    "<h4>a. Promoter and coding regions</h4>\n",
    "\n",
    "Download the genomic co-ordinates of promoter and coding sequences that are used in this analysis"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "%%bash\n",
    "\n",
    "wget http://bg.upf.edu/group/projects/tfbs/otherAnnotations/{promoter2500.bed.gz,cds.regions.gz} -P dataset"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As promoters, we considered the sequences upto 2.4kb upstream of TSS of all protein coding genes in GENCODE (v19). Promoter regions overlapping coding sequences (CDS) and UTRs were excluded."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h4>b. Genome sequence</h4>\n",
    "\n",
    "The genome sequence of hg19 version in fasta and the co-ordinates (as used in Khurana et al., 2013)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "%%bash\n",
    "\n",
    "mkdir dataset/genome\n",
    "wget http://bg.upf.edu/group/projects/tfbs/otherAnnotations/{human_g1k_v37.fasta.gz,hg19.genome} -P dataset/genome\n",
    "gzip -d dataset/genome/hg19.genome.gz"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h4>c. Mappability</h4>\n",
    "\n",
    "<ul>\n",
    "<li> UCSC Browser blacklisted regions (Duke and DAC, http://hgdownload.cse.ucsc.edu/goldenpath/hg19/encodeDCC/wgEncodeMapability/)</li>\n",
    "<li> Unique mappability of sequencing reads (\"CRG Alignability 36' Track\", score == 1) (http://genome.ucsc.edu/cgi-bin/hgFileUi?db=hg19&g=wgEncodeMapability) </li>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "%%bash\n",
    "\n",
    "mkdir dataset/mappability\n",
    "wget http://bg.upf.edu/group/projects/tfbs/otherAnnotations/{Duke_DAC_exclude_bed.gz,wgEncodeCrgMapabilityAlign30mer_score1.bed.gz} \\\n",
    "-P dataset/mappability"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h4>d. Nucleosome positioning data</h4>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "%%bash\n",
    "\n",
    "mkdir dataset/nucleosome\n",
    "wget http://hgdownload.cse.ucsc.edu/goldenpath/hg19/encodeDCC/wgEncodeSydhNsome/wgEncodeSydhNsomeGm12878Sig.bigWig \\\n",
    "-P dataset/nucleosome\n",
    "bigWigToBedGraph dataset/nucleosome/wgEncodeSydhNsomeGm12878Sig.bigWig dataset/nucleosome/wgEncodeSydhNsomeGm12878Sig.bed\n",
    "gzip -9 dataset/nucleosome/wgEncodeSydhNsomeGm12878Sig.bed"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"xrseq\"><h3>5. Excision repair data </h3></a>\n",
    "\n",
    "The nucleotide excision repair map generated by Hu et al., 2015 has been obtained from  http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE67941"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "%%bash\n",
    "\n",
    "# create a directory to save the repair data\n",
    "mkdir dataset/xr-seq\n",
    "\n",
    "# download the bigWig files (GSE67941_*_UNIQUE_NORM_fixedStep_25.bw) from the \"Supplementary file\" table of\n",
    "# above link and save in dataset/xr-seq\n",
    "\n",
    "# convert bigWig file to bed format using bigWigToBedGraph\n",
    "for i in $(ls dataset/xr-seq/*.bw);do filename=`echo $i | cut -d \".\" -f1`; if [ ! -f ${filename}.bed ];then \\\n",
    "bigWigToBedGraph ${filename}.bw ${filename}.bed;gzip -9 ${filename}.bed fi;done;"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.4.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
