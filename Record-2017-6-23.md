# Record-2017-6-23

昨晚对计算突变计数矩阵函数进行了优化，预先创建矩阵的想法可行但计算量太大，改为直接用单个变量（序列名+TSS位点）进行循环计算，函数内部临时创建矩阵的每一行，根据计算对该行相应的数据填入计数结果。

关于大的数据集一定要注意预先采样小数据集进行调试。调试完成后再进行运算。

学会用R简单调试bug。利用数量较少的`MT`染色体数据进行调试，结果正确。

